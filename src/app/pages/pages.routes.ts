import { RxjsComponent } from './rxjs/rxjs.component';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graph1Component } from './graph1/graph1.component';
import { AccoutSettingsComponent } from './accout-settings/accout-settings.component';
import { PromesasComponent } from './promesas/promesas.component';


const pagesRoutes: Routes=[
    { 
        path: '', component: PagesComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent, data:{titulo: 'Dashboard'}},
            { path: 'progress', component: ProgressComponent, data:{titulo: 'Progress Bar'}},
            { path: 'graph1', component: Graph1Component, data:{titulo: 'Graficas'}},
            { path: 'promesas', component: PromesasComponent, data:{titulo: 'Promesas'}},
            { path: 'rxjs', component: RxjsComponent, data:{titulo: 'RXJS'}},
            { path: 'account-settings', component: AccoutSettingsComponent, data:{titulo: 'Settings'}},
            { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        ]
    },
]

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );