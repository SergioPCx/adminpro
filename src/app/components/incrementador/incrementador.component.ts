import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: []
})
export class IncrementadorComponent implements OnInit {
  @ViewChild('txtProgress') txtProgress: ElementRef;  
  @Input('nombre') leyenda: string = 'Leyenda';
  @Input() percent: number = 50;

  @Output('ValueUpdate') changeValue: EventEmitter<number> = new EventEmitter(); 

  constructor() { }

  ngOnInit() {
  }

  onChange(newValue:number){
    // let elemHtml:any = document.getElementsByName('percent')[0]

    if(newValue>=100)
        this.percent=100
      else if(newValue<=0){
        this.percent=0
      }else{
        this.percent = newValue;
      }

      // elemHtml.value = this.percent;
      this.txtProgress.nativeElement.value = this.percent;
      this.changeValue.emit(this.percent);      
  }
  
  cambiarValor(valor:number){
    let _valor = this.percent +valor;
    if(_valor>=0 && _valor<=100){
      this.percent = _valor
      this.changeValue.emit(this.percent);
    }else{
      return
    }
    this.txtProgress.nativeElement.focus();
  }

}
