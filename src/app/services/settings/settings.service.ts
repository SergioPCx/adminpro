import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class SettingsService {

ajustes:Ajustes={
  temaUrl:"assets/css/colors/default-css",
  tema:"default"
}

  constructor(@Inject(DOCUMENT) private _document) {
    this.cargarAjustes();
   }

  guardarAjustes(){
    localStorage.setItem('settings', JSON.stringify(this.ajustes))
  }

  cargarAjustes(){
    if(localStorage.getItem('settings')){
      this.ajustes = JSON.parse(localStorage.getItem('settings'));
      this.aplicarTema(this.ajustes.tema)      
    }else{
      //valores por defecto
      this.aplicarTema(this.ajustes.tema)
    }
  }

  aplicarTema(tema:string){
    let url=`assets/css/colors/${tema}.css`;
    this._document.getElementById('theme').setAttribute('href', url);

    this.ajustes.tema = tema;
    this.ajustes.temaUrl = url;
    this.guardarAjustes();
  }

}


interface Ajustes{
  temaUrl: string;
  tema:string;
}